import "./App.css";
import Header from "./components/Header";
import useRouteElements from "./useRouteElements";
import { CorpusProvider } from "./context/CorpusContext";

function App() {
    const routeElements = useRouteElements();

    return (
        <>
            <CorpusProvider>
                <Header />
                <div className="container">{routeElements}</div>
            </CorpusProvider>
        </>
    );
}

export default App;
