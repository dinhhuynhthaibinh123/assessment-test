import { useRoutes } from "react-router-dom";
import SearchPage from "./pages/SearchPage";

export default function useRouteElements() {
    const routeElements = useRoutes([
        {
            path: "/",
            element: (
                <>
                    <SearchPage />
                </>
            ),
        },
        {
            path: "/about",
            element: <div>About me</div>,
        },
    ]);
    return routeElements;
}
