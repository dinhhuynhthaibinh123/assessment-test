import { useContext } from "react";
import SearchForm from "../components/SearchForm";
import SearchList from "../components/SearchList";
import CorpusContext from "../context/CorpusContext";
import corpusApi from "../apis/corpus.api";
import { toast } from "react-toastify";

export default function SearchPage() {
    const { search } = useContext(CorpusContext);

    const onSearch = async (text: string) => {
        search(text);
    };

    const onUpdate = async (text: string) => {
        try {
            const response = await corpusApi.update(text);
            toast.success(response.data.message);
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const responseError = error as any;
            console.log(responseError.response?.data);
            toast.error(responseError.response?.data.message);
        }
    };

    const onRemove = async (text: string) => {
        try {
            const response = await corpusApi.remove(text);
            toast.success(response.data.message);
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const responseError = error as any;
            console.log(responseError.response?.data);
            toast.error(responseError.response?.data.message);
        }
    };

    return (
        <>
            <div className="search-wrapper">
                <div className="search-left">
                    <SearchForm
                        title="Write a single word"
                        titleButton="search"
                        onSubmit={onSearch}
                    />
                    <SearchList />
                </div>
                <div className="search-right">
                    <SearchForm
                        title="Update the search corpus"
                        titleButton="Update"
                        onSubmit={onUpdate}
                    />
                    <SearchForm
                        title="Remove the similar word"
                        titleButton="Remove"
                        onSubmit={onRemove}
                    />
                </div>
            </div>
        </>
    );
}
