type Props = {
    item: string;
};

const FeedbackItem = ({ item }: Props) => {
    return (
        <div className="search-item">
            <div className="text-display">{item}</div>
        </div>
    );
};

export default FeedbackItem;
