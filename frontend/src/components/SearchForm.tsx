import { useState } from "react";
import Button from "./shared/Button";
import Card from "./shared/Card";

type Props = {
    title: string;
    titleButton: string;
    onSubmit: (text: string) => void;
};

export default function SearchForm({ title, titleButton, onSubmit }: Props) {
    const [text, setText] = useState("");
    const [btnDisabled, setBtnDisabled] = useState(true);
    const [message, setMessage] = useState("");

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const regex = /^[A-Za-z]+$/;

        if (text === "") {
            setBtnDisabled(true);
        } else if (regex.test(e.target.value) === false) {
            setMessage("Text must be a single word");
            setBtnDisabled(true);
        } else {
            setMessage("");
            setBtnDisabled(false);
        }
        setText(e.target.value);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        // console.log("handleSubmit", text);
        setText("");
        setBtnDisabled(true);
        onSubmit(text);
    };

    return (
        <Card reverse={false}>
            <form onSubmit={handleSubmit}>
                <h2>{title}</h2>
                <div className="input-group">
                    <input
                        onChange={handleTextChange}
                        type="text"
                        placeholder="Write a word"
                        value={text}
                    />
                    <Button
                        type="submit"
                        version="secondary"
                        isDisabled={btnDisabled}
                    >
                        {titleButton}
                    </Button>
                </div>
                {message ? (
                    <div className="message">{message}</div>
                ) : (
                    <div
                        className="message"
                        style={{
                            visibility: "hidden",
                        }}
                    >
                        {"Hidden"}
                    </div>
                )}
            </form>
        </Card>
    );
}
