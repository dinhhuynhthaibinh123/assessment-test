import { useContext } from "react";
import SearchItem from "./SearchItem";
import Spinner from "./shared/Spinner";
import CorpusContext from "../context/CorpusContext";

export default function SearchList() {
    const { words, isLoading } = useContext(CorpusContext);

    if (!isLoading && (!words || words.length === 0)) {
        return <p>No Result</p>;
    }

    return isLoading ? (
        <Spinner />
    ) : (
        <div className="search-list">
            {words.map((item, index) => (
                <SearchItem key={index} item={item} />
            ))}
        </div>
    );
}
