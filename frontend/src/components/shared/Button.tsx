import React from "react";

type Props = {
    children: React.ReactNode;
    version: string;
    type: "button" | "submit" | "reset";
    isDisabled: boolean;
};

export default function Button({
    children,
    version = "primary",
    type = "button",
    isDisabled = false,
}: Props) {
    return (
        <button
            type={type}
            disabled={isDisabled}
            className={`btn btn-${version}`}
        >
            {children}
        </button>
    );
}
