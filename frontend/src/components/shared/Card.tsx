type Props = {
    children: React.ReactNode;
    reverse: boolean;
};

export default function Card({ children, reverse }: Props) {
    return <div className={`card ${reverse && "reverse"}`}>{children}</div>;
}
