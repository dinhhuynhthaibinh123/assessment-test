import spinner from "../../assets/spinner3.gif";

export default function Spinner() {
    return (
        <img
            src={spinner}
            alt="Loading..."
            style={{
                width: "100px",
                margin: "auto",
                display: "block",
            }}
        />
    );
}
