export default function Header() {
    return (
        <header>
            <div className="container">
                <h2>Search Engine</h2>
            </div>
        </header>
    );
}
