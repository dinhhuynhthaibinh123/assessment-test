import http from "../utils/http";
import { SuccessResponse } from "../types/utils.type";

const URL = "corpus";

const corpusApi = {
    search(query: string) {
        return http.get<SuccessResponse<string[]>>(`${URL}/?search=${query}`);
    },
    update(word: string) {
        return http.post<SuccessResponse<null>>(`${URL}/`, { word });
    },
    remove(word: string) {
        return http.delete<SuccessResponse<null>>(`${URL}/${word}`);
    },
};

export default corpusApi;
