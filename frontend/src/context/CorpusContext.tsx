import React, { createContext, useState } from "react";
import corpusApi from "../apis/corpus.api";

type CorpusContextType = {
    words: string[];
    search: (text: string) => void;
    isLoading: boolean;
};

const defaultContextValue: CorpusContextType = {
    words: [],
    search: (text: string) => {
        console.log("Default search", text);
    },
    isLoading: false,
};

const CorpusContext = createContext<CorpusContextType>(defaultContextValue);

type CorpusProviderProps = {
    children: React.ReactNode;
};

export function CorpusProvider({ children }: CorpusProviderProps) {
    const [isLoading, setIsLoading] = useState(false);
    const [words, setWords] = useState<string[]>([]);

    const search = async (text: string) => {
        setIsLoading(true);
        try {
            const response = await corpusApi.search(text);

            setWords(response.data.data);
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const responseError = error as any;
            console.log(responseError.response?.data);
        } finally {
            setTimeout(() => {
                setIsLoading(false);
            }, 500);
        }
    };

    const contextValue: CorpusContextType = {
        words,
        search,
        isLoading,
    };

    return (
        <CorpusContext.Provider value={contextValue}>
            {children}
        </CorpusContext.Provider>
    );
}

export default CorpusContext;
