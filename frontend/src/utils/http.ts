import axios, { type AxiosInstance } from "axios";

const config = {
    baseUrl: "http://localhost:3000/",
};

export class Http {
    instance: AxiosInstance;

    constructor() {
        this.instance = axios.create({
            baseURL: config.baseUrl,
            timeout: 10000,
        });
    }
}
const http = new Http().instance;
export default http;
