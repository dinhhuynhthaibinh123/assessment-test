export interface SuccessResponse<Data> {
    message: string;
    data: Data;
}
