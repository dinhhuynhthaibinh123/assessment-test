import express from 'express'
import cors from 'cors'
import corpusRouter from './routers/corpus.routes'
import { defaultErrorHandler } from './middlewares/error.middlewares'

const PORT = process.env.PORT || 3000
const app = express()
app.use(cors())

app.use(express.json())

app.use('/corpus', corpusRouter)

app.use(defaultErrorHandler)
app.listen(PORT, () => {
  console.log('Server listening on port 3000')
})
