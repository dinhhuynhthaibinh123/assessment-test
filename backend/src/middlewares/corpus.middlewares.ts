import { ParamSchema, checkSchema } from 'express-validator'
import { CORPUS_MESSAGE } from '~/constants/messages'
import { validate } from '~/utils/validation'

const wordSchema: ParamSchema = {
  notEmpty: {
    errorMessage: CORPUS_MESSAGE.WORD_IS_REQUIRED
  },
  isString: {
    errorMessage: CORPUS_MESSAGE.WORD_IS_STRING
  },
  trim: true,

  isLength: {
    options: {
      min: 1,
      max: 20
    },
    errorMessage: CORPUS_MESSAGE.WORD_LENGTH_MUST_BE_FROM_1_TO_20
  },
  custom: {
    options: (value: string) => {
      const regex = /^[A-Za-z]+$/
      const isSingleWord = regex.test(value)

      if (!isSingleWord) {
        throw new Error(CORPUS_MESSAGE.WORD_IS_SINGLE_WORD)
      }

      return true
    }
  }
}

export const searchValidator = validate(
  checkSchema(
    {
      search: wordSchema
    },
    ['query']
  )
)

export const updateValidator = validate(
  checkSchema(
    {
      word: wordSchema
    },
    ['body']
  )
)

export const deleteValidator = validate(
  checkSchema(
    {
      word: wordSchema
    },
    ['params']
  )
)
