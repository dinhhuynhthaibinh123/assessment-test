export const CORPUS_MESSAGE = {
  WORD_IS_REQUIRED: 'Word is required',
  WORD_IS_STRING: 'Word must be a string',
  WORD_IS_SINGLE_WORD: 'Word must be a single word',
  WORD_LENGTH_MUST_BE_FROM_1_TO_20: 'Word length must be from 1 to 20',
  SEARCH_SUCCESS: 'Search Success',
  UPDATE_SUCCESS: 'Update Success',
  REMOVE_SUCCESS: 'Remove Success'
}
