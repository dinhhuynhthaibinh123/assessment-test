import Fuse from 'fuse.js'
import Cache from '~/models/Cache'
import { readAndSplitUniqueWords } from './file'

const cache = new Cache<string, string[]>()

// Read word list from file
const path = '../corpus/hemingway.txt'
let wordList: string[] = readAndSplitUniqueWords(path)

// Create Fuse instance with options
const fuseOptions: Fuse.IFuseOptions<{ word: string }> = {
  shouldSort: true,
  threshold: 0.2,
  keys: ['word']
}
const fuse = new Fuse(
  wordList.map((word) => ({ word })),
  fuseOptions
)

export const findSimilarWordsFuse = (query: string): string[] => {
  // Check if the query is in cache
  if (cache.has(query)) {
    return cache.get(query)!
  }

  // Find similar words
  const results = fuse.search(query)
  const similarWords = results.slice(0, 3).map((result) => result.item.word)

  // Cache the result for future queries
  cache.set(query, similarWords)

  return similarWords
}

export const addWordToCorpus = (newWord: string) => {
  // Add new word to wordList and update the Fuse instance
  wordList.push(newWord)
  fuse.setCollection(wordList.map((word) => ({ word })))

  const results = fuse.search(newWord)
  const similarWords = results.slice(0, 3).map((result) => result.item.word)

  // Update cache
  cache.set(newWord, similarWords)
}

export const removeSimilarWord = (targetWord: string) => {
  const results = fuse.search(targetWord)

  if (results.length > 0) {
    const similarWordToRemove = results[0].item.word

    // Remove the similar word from wordList and cache
    wordList = wordList.filter((word) => word !== similarWordToRemove)

    // Update the Fuse instance
    fuse.setCollection(wordList.map((word) => ({ word })))

    // Remove cached items containing similarWordToRemove
    for (const key of cache.keys()) {
      const value = cache.get(key)
      if (value && value.includes(similarWordToRemove)) {
        cache.delete(key)
      }
    }
  }
}
