import express from 'express'
import { validationResult, ValidationChain } from 'express-validator'
import { RunnableValidationChains } from 'express-validator/src/middlewares/schema'

export const validate = (validation: RunnableValidationChains<ValidationChain>) => {
  return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    await validation.run(req)
    const errors = validationResult(req)

    // If there are no errors, go to the next middleware
    if (errors.isEmpty()) {
      return next()
    }

    const errorsObject = errors.mapped() // { email: { msg: 'Email is required' } }

    next(errorsObject)
  }
}
