import fs from 'fs'

const readAndSplitUniqueWords = (filePath: string): string[] => {
  const wordList = fs.readFileSync(filePath, 'utf-8')
  const words = new Set<string>()
  const lines = wordList.split('\n')

  lines.forEach((line) => {
    // Remove special characters
    const cleanedLine = line.replace(/[^a-zA-Z0-9\s-]/g, '')

    // Convert to lowercase, split by whitespace, and remove empty strings
    const lineWords = cleanedLine
      .toLowerCase()
      .split(/\s+/)
      .filter((word) => word !== '')

    lineWords.forEach((word) => words.add(word))
  })

  return Array.from(words)
}

export { readAndSplitUniqueWords }
