import { Router } from 'express'
import { deleteController, searchController, updateController } from '~/controllers/corpus.controllers'
import { deleteValidator, searchValidator, updateValidator } from '~/middlewares/corpus.middlewares'
import { wrapRequestHandler } from '~/utils/handlers'
const corpusRouter = Router()

corpusRouter.get('/', searchValidator, wrapRequestHandler(searchController))
corpusRouter.post('/', updateValidator, wrapRequestHandler(updateController))
corpusRouter.delete('/:word', deleteValidator, wrapRequestHandler(deleteController))

export default corpusRouter
