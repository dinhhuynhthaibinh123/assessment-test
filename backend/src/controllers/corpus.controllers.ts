import { Request, Response, NextFunction } from 'express'
import { CORPUS_MESSAGE } from '~/constants/messages'
import { addWordToCorpus, findSimilarWordsFuse, removeSimilarWord } from '~/utils/fuse'

export const searchController = async (req: Request, res: Response, next: NextFunction) => {
  const similarWords = findSimilarWordsFuse(req.query.search as string)

  return res.json({ message: CORPUS_MESSAGE.SEARCH_SUCCESS, data: similarWords })
}

export const updateController = async (req: Request, res: Response, next: NextFunction) => {
  addWordToCorpus(req.body.word as string)

  return res.json({ message: CORPUS_MESSAGE.UPDATE_SUCCESS })
}

export const deleteController = async (req: Request, res: Response, next: NextFunction) => {
  removeSimilarWord(req.params.word as string)

  return res.json({ message: CORPUS_MESSAGE.REMOVE_SUCCESS })
}
